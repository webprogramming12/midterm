import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Products";

export const useProductStore = defineStore("product", () => {
  const sum = ref(0);

  const products = ref(
    Array.from(Array(100).keys()).map((item) => {
      return {
        id: item,
        name: "Product " + (item + 1),
        price: (Math.floor(Math.random() * 100) + 1) * 10,
      };
    })
  );

  function addMyOrders(order: Product): void {
    myOrders.value.push(order);
    sum.value += order.price;
  }

  // const orders = ref<Product[]>([]);

  const myOrders = ref<Product[]>([]);
  return { myOrders, products, addMyOrders, sum };
});
